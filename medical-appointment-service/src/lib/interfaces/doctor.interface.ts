export interface Horario {
	horaInicio: string;
	horaFin: string;
}

export interface FechasDisponible {
	fecha: string;
	horarios: Horario[];
}

export interface IDoctor {
	id: number;
	nombres: string;
	apellidos: string;
	especialidad: string;
	fechasDisponibles: FechasDisponible[];
}