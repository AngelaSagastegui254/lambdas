const axios = require('axios').default;

export async function getMedicalAppointmentsByPatient(
  patientId: number
): Promise<any> {
  console.log('[patientId]', patientId);
  const url = `http://23.23.15.212/citas-medicas/por-paciente/${patientId}`;
  const response = await axios.get(url);

  const appointments = response.data.result;
  console.log('[appointments]', appointments);
  return appointments;
}

export async function deleteMedicalAppointment(
  appointmentId: number
): Promise<any> {
  console.log('[appointmentId]', appointmentId);
  const url = `http://23.23.15.212/citas-medicas/${appointmentId}`;
  const response = await axios.delete(url);

  const appointment = response.data.result;
  console.log('[appointment deleted]', appointment);
  return appointment;
}
