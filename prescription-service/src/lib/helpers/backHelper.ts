const axios = require('axios').default;

export async function getPrescriptionsByPatient(
  patientId: number
): Promise<any> {
  console.log('[patientId]', patientId);
  const url = `http://23.23.15.212/recetas-medicas/por-paciente/${patientId}`;
  const response = await axios.get(url);

  const prescriptions = response.data.result;
  console.log('[prescriptions]', prescriptions);
  return prescriptions;
}

export async function getPrescriptionDetail(
  prescriptionId: number
): Promise<any> {
  console.log('[prescriptionId]', prescriptionId);
  const url = `http://23.23.15.212/recetas-medicas/detalle/${prescriptionId}`;
  const response = await axios.get(url);

  const prescription = response.data.result;
  console.log('[prescription detail]', prescription);
  return prescription;
}
