import { LexEvent, Message } from '../interfaces/IPrescriptionLambda.interface';

export function close(intentRequest: LexEvent, message: Message) {
  intentRequest.sessionState.intent.state = 'Fulfilled';

  const response = {
    sessionState: {
      sessionAttributes: {},
      dialogAction: {
        type: 'Close',
      },
      intent: intentRequest['sessionState']['intent'],
    },
    messages: [message],
  };
  console.log('[Response]', response);
  return response;
}
