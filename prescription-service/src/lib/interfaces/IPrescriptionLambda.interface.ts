// Interfaces to Intent request
export interface Bot {
  aliasId: string;
  aliasName: string;
  id: string;
  localeId: string;
  name: string;
  version: string;
}

export interface ReceiptIndex {
  value: {
    orginalValue: string;
  };
}

export interface ShowDetailSlot {
  receiptIndex: ReceiptIndex;
}

export interface UserIdInfo {
  value: {
    originalValue: string;
  };
}

export interface ShowListSlot {
  userId: UserIdInfo;
}

export type PrescriptionSlot = ShowListSlot | ShowDetailSlot;

export interface Intent {
  confirmationState: string; //'Confirmed' | 'Denied';
  name: string; //'ListMedicalReceipt' | 'ShowDetails'
  slots: ShowListSlot | ShowDetailSlot;
  state: string;
}

export interface Interpretation {
  intent: Intent;
  nluConfidence: number;
}

export interface SessionAttribute {}

export interface SessionState {
  dialogAction: DialogAction;
  intent: Intent;
  originatingRequestId: string;
  sessionAttributes: SessionAttribute;
}

export interface LexEvent {
  bot: Bot;
  inputMode: string;
  inputTranscript: string;
  interpretations: Interpretation[];
  invocationSource: string;
  messageVersion: string;
  responseContentType: string;
  sessionId: string;
  sessionState: SessionState;
}

//Interface to Lambda Response
export interface Message {
  content: string;
  contentType: string;
}

export interface DialogAction {
  type: string;
}

export interface LambdaResponse {
  interpretations?: Interpretation[];
  messages: Message[];
  sessionId?: string;
  sessionState: SessionState;
}
