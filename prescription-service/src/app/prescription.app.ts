import morgan = require('morgan');
import cors = require('cors');
import express = require('express');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(morgan('tiny'));

export = app;
