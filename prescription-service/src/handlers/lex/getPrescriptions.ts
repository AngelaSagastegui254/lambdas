import {
  LexEvent,
  PrescriptionSlot,
} from '../../lib/interfaces/IPrescriptionLambda.interface';
import { close } from '../../lib/helpers/lambdaHelper';
import {
  getPrescriptionDetail,
  getPrescriptionsByPatient,
} from '../../lib/helpers/backHelper';

async function handlerList(intentRequest: LexEvent, callback: any) {
  const intent = intentRequest.sessionState.intent;
  const slot: PrescriptionSlot = intent.slots;
  const patientId = Number(slot['userId']['value']['interpretedValue']);

  const prescriptions = await getPrescriptionsByPatient(patientId);
  let list = 'Estas son las últimas 5 recetas médicas. \n';
  prescriptions.forEach((prescription, index) => {
    list += `${index + 1} ${prescription.doctor.especialidad} ${
      prescription.fecha
    }, `;
  });
  list = list.slice(0, list.length - 2);
  list += '. Ingrese el número de la receta que desea obtener más detalle.';
  const message = {
    contentType: 'PlainText',
    content: list,
  };

  return callback(close(intentRequest, message));
}

async function handlerShowDetail(intentRequest: LexEvent, callback: any) {
  const intent = intentRequest.sessionState.intent;
  const slot: PrescriptionSlot = intent.slots;

  const patientId = Number(slot['userId']['value']['interpretedValue']);
  const prescriptions = await getPrescriptionsByPatient(patientId);
  const prescriptionIndex = Number(
    slot['receiptIndex']['value']['interpretedValue']
  );
  const prescriptionId = prescriptions[prescriptionIndex - 1].id;

  const prescription = await getPrescriptionDetail(prescriptionId);

  let detail = `${prescription.especialidad} ${prescription.fecha}. `;
  let products = '';
  prescription.productos.forEach((producto) => {
    products += `${producto.cantidad} ${producto.nombre} ${producto.indicacion}, `;
  });
  products = products.slice(0, products.length - 2);
  detail += `${products}.`;

  const message = {
    contentType: 'PlainText',
    content: detail,
  };
  return callback(close(intentRequest, message));
}

async function dispatch(intentRequest: LexEvent, callback: any) {
  console.log('[First log]', JSON.stringify(intentRequest));

  const intent = intentRequest.sessionState.intent;
  const intentName = intent.name;

  if (intentName === 'ListMedicalReceipt') {
    await handlerList(intentRequest, callback);
  } else if (intentName === 'ShowDetails') {
    await handlerShowDetail(intentRequest, callback);
  }

  const message = {
    contentType: 'PlainText',
    content: 'Hola!!',
  };
  return callback(close(intentRequest, message));
}

exports.handler = async (
  event: LexEvent,
  context: any,
  callback: (error: any, response?: any) => any
) => {
  try {
    await dispatch(event, (response) => {
      callback(null, response);
    });
  } catch (err) {
    callback(err);
  }
};
